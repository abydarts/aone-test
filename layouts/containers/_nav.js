export default [
  {
    _name: 'CSidebarNav',
    _children: [
      {
        _name: 'CSidebarNavItem',
        name: 'About',
        to: '/about',
        icon: 'mdi-information-outline'
      },
      {
        _name: 'CSidebarNavItem',
        name: 'User',
        to: '/users',
        icon: 'mdi-account-multiple'
      }
    ]
  }
]
